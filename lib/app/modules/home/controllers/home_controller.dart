import 'package:get/get.dart';
import 'package:smartfarming/app/modules/signin/views/signin_view.dart';
import 'package:smartfarming/app/modules/signup/views/signup_view.dart';
import 'package:smartfarming/app/utils/secure_storage.dart';

class HomeController extends GetxController {
  //TODO: Implement HomeController
  final storage  = new SecureStorage();

  final count = 0.obs;
  final email = ''.obs;

  @override
  void onInit() {
    getDataEmail();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void getDataEmail() async{
    email.value = (await storage.getEmail())!;
  }

  void logout(){
    storage.delete();
    Get.offAll(SigninView());
  }
}
