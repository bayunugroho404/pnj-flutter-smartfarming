import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:smartfarming/app/modules/gps/views/gps_view.dart';
import 'package:smartfarming/app/services/databse.dart';
import 'package:smartfarming/app/utils/size_config.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import '../controllers/home_controller.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  final controller = Get.put(HomeController());
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  late List<SalesData> _chartData;
  late List<KelembapanData> _chartDataK;
  List<SalesData> list= [];
  List<KelembapanData> list_kelembapan= [];

  @override
  void initState() {
    // TODO: implement initState
    _chartData = getChartData();
    _chartDataK = getChartDataKelembapan();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    CollectionReference suhu = firestore.collection("suhu");
    CollectionReference kelembapan = firestore.collection("kelembapan");

    controller.getDataEmail();
    return Scaffold(
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            list.length > 0?UserAccountsDrawerHeader(
              accountName: Text("Hello,Selamat Datang"),
              accountEmail: Obx(() => Text("${controller.email.value}")),
              currentAccountPicture: CircleAvatar(
                backgroundColor:
                Theme.of(context).platform == TargetPlatform.iOS
                    ? Colors.blue
                    : Colors.white,
                child: CircleAvatar(
                  backgroundImage: AssetImage("assets/pnj.png"),
                ),
              ),
            ):Container(),
            ListTile(
              title: Text("Suhu Dan Kelembapan"),
              trailing: Icon(Icons.arrow_forward),
            ),
            ListTile(
              onTap: (){
                Get.to(GpsView());
              },
              title: Text("GPS"),
              trailing: Icon(Icons.arrow_forward),
            ),
            Container(
              height: SizeConfig.screenHight / 1.8,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: ListTile(
                  onTap: (){
                    controller.logout();
                  },
                  title: Text("Logout"),
                  trailing: Icon(Icons.exit_to_app),
                ),
              ),
            ),
          ],
        ),
      ),
      appBar: AppBar(
        title: Text('Suhu dan Kelembapan'),
        centerTitle: true,
        actions: [
          IconButton(icon: Icon(Icons.refresh), onPressed: (){Get.offAll(HomeView());})
        ],
      ),
      body: Container(
        height: SizeConfig.screenHight,
        width: SizeConfig.screenWidth,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                'History Suhu',
                style: TextStyle(
                    fontSize: SizeConfig.blockVertical * 5),
              ),
              Divider(),
              Container(
                height: SizeConfig.screenHight / 4,
                child: SfCartesianChart(
                    legend: Legend(isVisible: true,position:LegendPosition.bottom),
                    primaryXAxis:
                    CategoryAxis(title: AxisTitle(text: '')),
                    axes: <ChartAxis>[
                      CategoryAxis(
                          name: 'xAxis',
                          opposedPosition: true),
                    ],
                    series: <CartesianSeries>[
                      ColumnSeries<SalesData, String>(
                          name: 'Suhu (Celcius)',
                          dataSource: _chartData,
                          xValueMapper: (SalesData sales, _) => sales.salesMonth,
                          yValueMapper: (SalesData sales, _) => sales.sales),

                    ]),
              ),
              Text(
                'History Kelembapan',
                style: TextStyle(
                    fontSize: SizeConfig.blockVertical * 5),
              ),
              Divider(),
              Container(
                height: SizeConfig.screenHight / 4,
                child: SfCartesianChart(
                    legend: Legend(isVisible: true,position:LegendPosition.bottom),
                    primaryXAxis:
                    CategoryAxis(title: AxisTitle(text: '')),
                    axes: <ChartAxis>[
                      CategoryAxis(
                          name: 'xAxis',
                          opposedPosition: true),
                    ],
                    series: <CartesianSeries>[
                      ColumnSeries<KelembapanData, String>(
                          name: 'Kelembapan (%)',
                          dataSource: _chartDataK,
                          xValueMapper: (KelembapanData sales, _) => sales.salesMonth,
                          yValueMapper: (KelembapanData sales, _) => sales.sales),

                    ]),
              ),
              Text(
                'Keterangan',
                style: TextStyle(
                    fontSize: SizeConfig.blockVertical * 5),
              ),
              Divider(),
              Row(
                children: [
                  Container(
                    height: 100,
                    color: Colors.grey,
                    width: SizeConfig.screenWidth/2,
                    child:StreamBuilder<QuerySnapshot>(
                        stream: suhu.limit(1).snapshots(),
                        builder: (_, AsyncSnapshot<QuerySnapshot> snapshot) {
                          if (!snapshot.hasData) {
                            return Center(child: CircularProgressIndicator());
                          } else {
                            return ListView(
                              shrinkWrap: true,
                              children: snapshot.data!.docs
                                  .map((DocumentSnapshot document) {
                                return Container(
                                  margin: EdgeInsets.all(4),
                                  child: Column(
                                    children: [
                                      Text(
                                        'Suhu',
                                        style: TextStyle(
                                            fontSize: SizeConfig.blockVertical * 5,
                                            ),
                                      ),
                                      Text(
                                        '${(document.data() as dynamic)["suhu"]}c',
                                        style: TextStyle(
                                            fontSize: SizeConfig.blockVertical * 4,
                                            color: Colors.red,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                );
                              }).toList(),
                            );
                          }
                        }),
                  ),
                  Container(
                    height: 100,
                    color: Colors.green,
                    width: SizeConfig.screenWidth/2,
                    child:StreamBuilder<QuerySnapshot>(
                        stream: kelembapan.limit(1).snapshots(),
                        builder: (_, AsyncSnapshot<QuerySnapshot> snapshot) {
                          if (!snapshot.hasData) {
                            return Center(child: CircularProgressIndicator());
                          } else {
                            return ListView(
                              shrinkWrap: true,
                              children: snapshot.data!.docs
                                  .map((DocumentSnapshot document) {
                                return Container(
                                  margin: EdgeInsets.all(4),
                                  child: Column(
                                    children: [
                                      Text(
                                        'Kelembapan',
                                        style: TextStyle(
                                            fontSize: SizeConfig.blockVertical * 5,
                                            ),
                                      ),
                                      Text(
                                        '${(document.data() as dynamic)["kelembapan"]}%',
                                        style: TextStyle(
                                            fontSize: SizeConfig.blockVertical * 4,
                                            color: Colors.red,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                );
                              }).toList(),
                            );
                          }
                        }),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  List<SalesData> getChartData() {
    list.clear();
    Database().getSuhu().then((value) {
      value.docs.forEach((element) {
        setState(() {
          list.add(new
          SalesData('${element['tgl']}',  double.parse('${element['suhu']
              .toString().replaceAll("c", "")}'),
              double.parse('${element['suhu']
                  .toString().replaceAll("c", "")}')),
          );
        });
      });
    });
   return list;
  }
  List<KelembapanData> getChartDataKelembapan() {
    list_kelembapan.clear();
    Database().getKelembapan().then((value) {
      value.docs.forEach((element) {
        setState(() {
          list_kelembapan.add(new
          KelembapanData('${element['tgl']}',  double.parse('${element['kelembapan']
              .toString().replaceAll("%", "")}'),
              double.parse('${element['kelembapan']
                  .toString().replaceAll("c", "")}')),
          );
        });
      });
    });
   return list_kelembapan;
  }
}


class SalesData {
  SalesData(this.salesMonth, this.sales, this.profit);
  final String salesMonth;
  final double sales;
  final double profit;
}

class KelembapanData {
  KelembapanData(this.salesMonth, this.sales, this.profit);
  final String salesMonth;
  final double sales;
  final double profit;
}