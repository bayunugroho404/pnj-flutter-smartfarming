import 'package:cloud_firestore/cloud_firestore.dart';

class Database {
  FirebaseFirestore firestore = FirebaseFirestore.instance;

  Future<QuerySnapshot> getDataUser(String email) {
    return FirebaseFirestore.instance
        .collection('users')
        .where("email", isEqualTo: email)
        .limit(1)
        .get();
  }

  Future<QuerySnapshot> getSuhu() {
    return FirebaseFirestore.instance
        .collection('suhu')
        .get();
  }
  Future<QuerySnapshot> getKelembapan() {
    return FirebaseFirestore.instance
        .collection('kelembapan')
        .get();
  }

  Future<bool> createNewUser(String name,String email,String password) async {
    try {
      await firestore.collection("users").add({
        "name":name,
        "email":email,
        "password":password,
      });
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }
}