import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:smartfarming/app/modules/signin/views/signin_view.dart';
import 'package:smartfarming/app/utils/constant.dart';
import 'package:smartfarming/app/utils/size_config.dart';
import 'package:smartfarming/app/widgets/toast.dart';

import '../controllers/signup_controller.dart';

class SignupView extends GetView<SignupController> {
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController noController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController passwordConfirmationController =
  TextEditingController();

  final controller = Get.put(SignupController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: SizeConfig.screenHight,
        width: SizeConfig.screenWidth,
        margin: EdgeInsets.all(8.0),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: SizeConfig.screenHight / 8,),
              Image.asset("assets/pnj.png",height: SizeConfig.screenHight /4),
              SizedBox(height: SizeConfig.screenHight / 12,),
              TextField(
                controller: nameController,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    hintText: "Name",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32.0))),
              ),
              SizedBox(height: 10.0),
              TextField(
                controller: emailController,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    hintText: "Email",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32.0))),
              ),
              SizedBox(height: 10.0),
              Visibility(
                visible: false,
                child: TextField(
                  controller: noController,
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      hintText: "Phone Number",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(32.0))),
                ),
              ),
              SizedBox(height: 10.0),
              TextField(
                obscureText: true,
                controller: passwordController,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    hintText: "Password",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32.0))),
              ),
              SizedBox(height: 10.0),
              TextField(
                obscureText: true,
                controller: passwordConfirmationController,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    hintText: "Password Confirmation",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32.0))),
              ),
              SizedBox(height: 25.0),
              Material(
                elevation: 5.0,
                borderRadius: BorderRadius.circular(30.0),
                color: primary,
                child: MaterialButton(
                  minWidth: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  onPressed: () {
                    if (passwordController.text !=
                        passwordConfirmationController.text) {
                      showToast("password tidak sama ");
                    } else {
                      controller.register(
                        nameController.text,
                        emailController.text,
                        passwordController.text,
                        passwordConfirmationController.text,
                      );
                    }
                  },
                  child: Text(
                    "Submit",
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              SizedBox(height: 20.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text('Sudah punya akun ? '),
                  InkWell(
                      onTap: () {
                        Get.offAll(SigninView());
                      },
                      child: Text(
                        'Login',
                        style: TextStyle(color: Colors.blue),
                      )),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
