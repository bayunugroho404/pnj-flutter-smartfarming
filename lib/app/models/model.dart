/**
 * Created by Bayu Nugroho
 * Copyright (c) 2021 . All rights reserved.
 */

class ModelSK {
  final String suhu;
  final String kelembapan;
  ModelSK(this.suhu,this.kelembapan);

  ModelSK.fromMap(Map<String, dynamic> map)
      : assert(map['suhu'] != null),
        assert(map['kelembapan'] != null),
        suhu = map['suhu'],
        kelembapan=map['kelembapan'];

  @override
  String toString() => "Record<$suhu:$kelembapan>";
}