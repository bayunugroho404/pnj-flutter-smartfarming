import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:smartfarming/app/modules/home/views/home_view.dart';
import 'package:smartfarming/app/services/databse.dart';
import 'package:smartfarming/app/utils/secure_storage.dart';

class SigninController extends GetxController {
  //TODO: Implement SigninController
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  final storage  = new SecureStorage();


  final count = 0.obs;
  @override
  void onInit() {
    // ever(count,(_) => print('ever'));
    // once(count,(_) => print('once'));
    checkUser();
    // debounce(count,(_) => print('debounce'));
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void login(String email ,String password){
    Database().getDataUser(email).then((val) {
      if(val.docs[0]['email'] == email && val.docs[0]['password'] == password){
        storage.persistEmail(email);
        Get.snackbar("Sukses", "Berhasil Login");
          Get.offAll(HomeView());
      }else{
        Get.snackbar("Ops", "akun tidak diketahui");
      }
    }).catchError((onError){
      Get.snackbar("Ops", "akun tidak diketahui");
    });
  }

  void checkUser() async {
    bool status_login = await storage.hasEmail();
    if (status_login ) {
      Get.offAll(HomeView());
    }
  }
}
