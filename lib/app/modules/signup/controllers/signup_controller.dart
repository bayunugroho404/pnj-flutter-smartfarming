import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:smartfarming/app/modules/signin/views/signin_view.dart';
import 'package:smartfarming/app/services/databse.dart';
import 'package:smartfarming/app/widgets/toast.dart';

class SignupController extends GetxController {
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  final count = 0.obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void increment() => count.value++;

  void register(String name, String email, String password,
      String passwordConfirmationController) {
    Database().getDataUser(email).then((val) {
      if (val.docs[0]['email'] == email) {
        Get.snackbar("Ops", "Email Sudah Terdaftar");
      }
    }).catchError((onError) {
      CollectionReference users = firestore.collection("users");
      users.add({"name": name, "email": email, "password": password});
      Get.snackbar("SUkses", "Registrasi Berhasil");
      showToast("BERHASIL REGISTER");
      Get.offAll(SigninView());
    });
  }
}
