import 'package:flutter/cupertino.dart';


//file constant to be accessible in many classes
const String STATUS_LOGIN = "STATUS_LOGIN";
const String TOKEN = "TOKEN";
const String USER_ID = "USER_ID";
const String IS_USER = "IS_USER";
const String PHONE_NUMBER = "PHONE_NUMBER";
const String EMAIL = "EMAIL";
const String BASE_URL = "http://192.168.1.9:8000";
const String BASE_URL_FCM = "https://fcm.googleapis.com/fcm/send";
const String SERVER_KEY = "key=AAAA72dQC30:APA91bHpJ3i4GzyscjRbDUBgtgkXMW5L5leD8Y4iyg-bdmqnjV9oqst6rGo4wmJkuu31tmOmFfKCdFkpE2mdmP_wOW7FTB7XKjs425ONKorNSbG19MiPDaw8AAKVrYHrWxHeVKmkKoYI ";
const String NAME = "NAME";
const String lorem = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
const Color  secondary= Color(0xfffad49a);
const Color primary = Color(0xff59981a);
const Color grey = Color(0xff998869);