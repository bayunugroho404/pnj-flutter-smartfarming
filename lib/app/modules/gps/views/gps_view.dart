import 'dart:async';
import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

import '../controllers/gps_controller.dart';

class GpsView extends StatefulWidget {
  @override
  _GpsViewState createState() => _GpsViewState();
}

class _GpsViewState extends State<GpsView> {
  final controller = Get.put(GpsController());
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  Uint8List? bytes;
  StreamSubscription? _locationSubscription;
  Location _locationTracker = Location();
  Marker? marker;
  Circle? circle;
  GoogleMapController? _controller;

  @override
  void initState() {
    super.initState();
    rootBundle.load('assets/drone.png').then((data) {
      if (mounted)
        setState(() {
          bytes = data.buffer.asUint8List();
        });
    });
  }

  @override
  Widget build(BuildContext context) {
    CollectionReference suhu = firestore.collection("maps");

    return Scaffold(
      appBar: AppBar(
        title: Text('GpsView'),
        centerTitle: true,
      ),
      body: StreamBuilder<QuerySnapshot>(
          stream: suhu.limit(1).snapshots(),
          builder: (_, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (!snapshot.hasData) {
              return Center(child: CircularProgressIndicator());
            } else {
              marker = Marker(
                  markerId: MarkerId("home"),
                  position: LatLng(
                      double.parse('${snapshot.data!.docs[0]['lat']}'),
                      double.parse('${snapshot.data!.docs[0]['lng']}')
                  ),
                  draggable: false,
                  zIndex: 2,
                  flat: true,
                  anchor: Offset(0.5, 0.5),
                  icon: BitmapDescriptor.fromBytes(bytes!));
              return GoogleMap(
                mapType: MapType.hybrid,
                initialCameraPosition: CameraPosition(
                    target: LatLng( double.parse('${snapshot.data!.docs[0]['lat']}'),
                        double.parse('${snapshot.data!.docs[0]['lng']}')),
                    zoom: 17),
                markers: Set.of((marker != null) ? [marker!] : []),
                circles: Set.of((circle != null) ? [circle!] : []),
                onMapCreated: (GoogleMapController controller) {
                  _controller = controller;
                },
              );
            }
          }),
    );
  }
}
